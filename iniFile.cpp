#include <iostream>
#include <map>
#include <cstdio>
 
int main(){
    int Level, Count, Target, Speed, Time;
    std::map<std::string, int*> vars{
        {"Level", &Level},
        {"Count", &Count},
        {"Target", &Target},
        {"Speed", &Speed},
        {"Time", &Time}
    };
    FILE *file = fopen("ini.txt", "r");
    char name[128];
    int value;
    while(fscanf(file, " %127[a-zA-Z] = %d", &name, &value) == 2)
        *vars[name] = value;
        
    for(auto& pair:vars)
        std::cout << pair.first << " " << *pair.second << std::endl;
    return 0;
}
