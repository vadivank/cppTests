#include <iostream>
#include <string>


int main(){

    int v1 = 5;
    int *ptr1 = &v1;
    int *ptr2;
    *ptr2 = 9;

    std::cout << "int *ptr = &v1;\n";
    std::cout << "v1==[" << v1 << "]\n";
    std::cout << "&v1==[" << &v1 << "]\n";
    std::cout << "*ptr1==[" << *ptr1 << "]\n";
    std::cout << "ptr1==[" << ptr1 << "]\n";
    std::cout << "sizeof(*ptr1)==[" << sizeof(*ptr1) << "]\n";
    std::cout << "sizeof(ptr1)==[" << sizeof(ptr1) << "]\n";
    std::cout << "\n\n\n";

    std::cout << "int *ptr2;\n";
    std::cout << "*ptr2 = 9;\n";
    std::cout << "ptr2==[" << ptr2 << "]\n";
    std::cout << "*ptr2==[" << *ptr2 << "]\n";
    std::cout << "\n\n\n";

    return 0;
}