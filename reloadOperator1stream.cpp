#include <iostream>
#include <sstream>
#include <iomanip>
// #include <algorithm>
// #include <vector>

// оператор вывода вывода в поток
// std::ostream& operator<<(std::ostream& s, const Obj& o);
//
// оператор ввода из потока
// std::istream& operator>>(std::istream& s, Obj& o);


struct Duration{
    int hour;
    int min;

    Duration(int h = 0, int m = 0){  // конструктор
        hour = h;
        min = m;
    }
};

// функция заменяется перегрузкой operator>>
// Duration readDuration(std::istream& stream){
//     int h = 0;
//     int m = 0;
//     stream >> h;
//     stream.ignore(1);
//     stream >> m;
//     return Duration{h, m};
// }
std::istream& operator>>(std::istream& stream, Duration& duration){
    stream >> duration.hour;
    stream.ignore(1);
    stream >> duration.min;
    return stream;    
}

// функция заменяется перегрузкой operator<<
// void printDuration(std::ostream& stream, const Duration& duration){
//     stream << std::setfill('0');
//     stream << std::setw(2) << duration.hour << ':'
//             << std::setw(2) << duration.min;
// }
std::ostream& operator<<(std::ostream& stream, const Duration& duration){
    stream << std::setfill('0');
    stream << std::setw(2) << duration.hour << ':'
            << std::setw(2) << duration.min;
    return stream;
}


int main(){
    operator<<(operator<<(std::cout, "hello"), " world"); // пример того, как работает оператор с потоком
    std::cout << "\n";

    std::stringstream dur_ss("02:50");
    Duration dur1;
    //Duration dur1 {0, 0};                 // заменили инициализацию объекта конструктором структуры
    //Duration dur1 = readDuration(dur_ss); // заменили ф-ю перегрузкой оператора >>
    //printDuration(std::cout, dur1);       // заменили ф-ю перегрузкой оператора <<
    
    dur_ss >> dur1;
    std::cout << dur1 << std::endl;


    return 0;
}
