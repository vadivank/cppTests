#include <iostream>
#include <sstream>
#include <iomanip>
// #include <algorithm>
// #include <vector>

// ===> Обработка данных в конструкторе, чтобы время выводилось корректно

class Duration{
private:
    int hour;
    int min;
public:
    Duration(){}        // конструктор по-умолчанию (без него не работает)
    Duration(int hour_, int min_){
        int total = hour_ * 60 + min_;
        hour = total / 60 ;
        min = total % 60;
    }
    int Hour() const {
        return hour;
    }
    int Min() const {
        return min;
    }
};

std::istream& operator>>(std::istream& in, Duration& duration){
    int hour;
    in >> hour;
    in.ignore(1);
    int min;
    in >> min;
    duration = Duration(hour, min);
    return in;    
}

std::ostream& operator<<(std::ostream& stream, const Duration& duration){
    stream << std::setfill('0');
    stream << std::setw(2) << duration.Hour() << ':'
            << std::setw(2) << duration.Min();
    return stream;
}

Duration operator+(const Duration& lhs, const Duration& rhs){
    return Duration{lhs.Hour() + rhs.Hour(), lhs.Min() + rhs.Min()};
}


int main(){
    std::stringstream dur_ss1("01:35");
    Duration dur1;
    dur_ss1 >> dur1;
    std::cout << "Time1: " << dur1 << std::endl;

    std::stringstream dur_ss2("02:35");
    Duration dur2;
    dur_ss2 >> dur2;
    std::cout << "Time2: " << dur2 << std::endl;

    std::cout << "Total: " << dur1 
            << " + " << dur2 
            << " = " << dur1 + dur2 
            << std::endl;

    return 0;
}
