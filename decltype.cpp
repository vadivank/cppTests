#include <iostream>

auto GetA(int a) -> decltype(a + 3.0){
    return a;
}

int main(){
    std::cout << GetA(2) << "\n";
    std::cout << sizeof(GetA(2)) << "\n";

    return 0;
}
