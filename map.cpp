﻿/*-------------------------------------------------------
*  C++ Example - Update value in map
*/

#include <map> // Include map
#include <iostream>
//using namespace std;

int main(){

      /* Create a hash map - int as key and string as value */
      std::map<int, std::string> m;

      /* Insert some elements ( key value pair) in the hash map */
      m.insert (std::make_pair<int, std::string>(1, "Peter"));
      m.insert (std::make_pair<int, std::string>(2, "Lisa"));
      m.insert (std::make_pair<int, std::string>(3, "Danish"));
      m.insert (std::make_pair<int, std::string>(4, "Linda"));

      /* Print the elements from hash map */
      std::cout << "map Contains:\n";   
      std::map<int, std::string>::iterator itr;
      for( itr = m.begin(); itr != m.end(); ++itr){
            std::cout << "Key => " << itr->first << ", Value => " << itr->second.c_str() << std::endl;
      }    

      /* Add / insert some elements ( key value pair) in the hash map */
      m.insert(std::make_pair<int, std::string>(8, "Michael")); // add data to red-black tree
      m.insert(std::make_pair<int, std::string>(3, "John")); // repeat by key ???

      std::cout << "Updated map Contains:\n";  

      for( itr = m.begin(); itr != m.end(); ++itr){
            std::cout << "Key => " << itr->first << ", Value => " << itr->second.c_str() << std::endl;      
      }  

      return 0;
}