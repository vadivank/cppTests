#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <fstream>
#include <map>

class Duration{
private:
    int hour;
    int min;
public:
    //Duration(){}        // конструктор по-умолчанию (можно удалить после определения в параметрическом конструкторе значений по-умолчанию)
    Duration(int hour_ = 0, int min_ = 0){
        int total = hour_ * 60 + min_;
        hour = total / 60 ;
        min = total % 60;
    }
    int Hour() const {
        return hour;
    }
    int Min() const {
        return min;
    }
};

bool operator<(const Duration& lhs, const Duration& rhs){
    if (lhs.Hour() == rhs.Hour()) {
        return lhs.Min() < rhs.Min();
    }
    return lhs.Hour() < rhs.Hour();
}

std::istream& operator>>(std::istream& in, Duration& duration){
    int hour;
    in >> hour;
    in.ignore(1);
    int min;
    in >> min;
    duration = Duration(hour, min);
    return in;    
}

std::ostream& operator<<(std::ostream& stream, const Duration& duration){
    stream << std::setfill('0');
    stream << std::setw(2) << duration.Hour() << ':'
            << std::setw(2) << duration.Min();
    return stream;
}

Duration operator+(const Duration& lhs, const Duration& rhs){
    return Duration{lhs.Hour() + rhs.Hour(), lhs.Min() + rhs.Min()};
}


int main(){
    std::ifstream input("reloadOperator4map.txt");
    Duration worst; // худший результат бегунов
    std::map<Duration, std::string> all;
    if(!input) return 1;
    Duration dur;
    std::string name;
    while(input >> dur >> name){
        if (worst < dur){
            worst = dur;
        }
        all[dur] += (name + " ");
    }
    
    std::ofstream out("reloadOperator4mapResult.txt");
    for(const auto durationNames : all){
        out << durationNames.first << "\t" 
            << durationNames.second << std::endl;
    }

    std::cout << "Worst runner: " << all[worst] << std::endl;


    return 0;
}
