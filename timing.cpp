#include <iostream>
#include <chrono>

class Timer {                                                       // замеряет время выполнения алгоритмов
private:
    using clock_t = std::chrono::high_resolution_clock;             //псевдонимы типов для удобного доступа к вложенным типам
    using second_t = std::chrono::duration<double, std::ratio<1> >;
    std::chrono::time_point<clock_t> m_beg;                         //начало отсчета времени
public:
    Timer() : m_beg(clock_t::now()){}                               //конструктор - при вызове объекта начало отсчета обнуляется
    void reset(){                                                   //обнулить время вручную
        m_beg = clock_t::now();
    }
    double elapsed() const {                                        //подсчет времени
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};

int main(){
    Timer t;    // запуск общего таймера (таймер t автоматически обнуляется)
    
    Timer a;    // запуск таймера a (таймер a автоматически обнуляется)
    /* some algorithm for timing a*/
    std::cout << "Algorithm a taken: " << a.elapsed() << " sec\n";  
    
    Timer b;    // запуск таймера b (таймер b автоматически обнуляется)
    /* some algorithm for timing b*/
    std::cout << "Algorithm b taken: " << b.elapsed() << " sec\n";  
    
    std::cout << "All file TIME taken: " << t.elapsed() << " sec\n";  
    return 0;
}
