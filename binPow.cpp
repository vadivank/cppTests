#include <iostream>

int binPowRec(int a, int n){
    if (n == 0) {
        return 1;
    }
    if (n % 2 == 1) {
        return binPowRec(a, n-1) * a;
    }
    else {
        int b = binPowRec(a, n/2);
        return b * b;
    }
}

int main(){
    std::cout << binPowRec(5, 3); // рекурсивно
    
    return 0;
}
