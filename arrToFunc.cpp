#include <iostream>


//передача массива в функцию
template<class T, size_t N>
void Fill(T(&arr)[N]){
    for (size_t i=0; i<N; ++i) arr[i]=i;
}


//возврат массива из функции
template<class T, size_t N>
T(&Get())[N]{
    static T arr [N];
    return arr;    
}


//функция принимает массив по ссылке и возвращает массив по ссылке
template<class T, size_t N>
T(&Example(T(&arr)[N]))[N]{
    Fill(arr);
    auto& dst = Get<T,N>();
    for(size_t i=0; i<N; ++i) dst[i]=arr[i]*10;
    return dst;
}

int main(){
    std::cout<<"Hello world!\n";
    int a[10] = {};
    const auto& result = Example(a);
    std::cout << "int result[10] = {";
    for(const auto &i:result) std::cout << i << ", ";
    std::cout << "};\n";
}