/*
#include <iostream>
#include <fstream>
#include <string.h>

int main(){
    std::string s = "ыА";
    char *c = new char[4];
    strcpy(c, s.c_str());
    std::cout << s.size();
    c[3] = 'A';
    s.assign(c, 4);
    std::cout << '\n' << s;
    return 0;
}
*/


#include <iostream>
#include <string>


void assignDemo(std::string str1, std::string str2){
    str1.assign(str2, 3, 5);
    std::cout << "After assign(): {" << str1 << "}\n";    
}

int main(){

    std::string str1 ("987654321");
    std::string str2 ("123456789");
    std::cout << "Original String: {" << str1 << "}\n";
    assignDemo(str1, str2);
 
    return 0;
}