#include <iostream>
#include <cstdarg>
#include <string>


int sum_c(int dataN, ...){
    va_list vaL;
    int s = 0;
    va_start (vaL, dataN);
    for(int i = 0; i < dataN; ++i) s+= va_arg(vaL, int);
    va_end(vaL);
    return s;
}


int sum_iL(std::initializer_list<int>lst){
    int s = 0;
    for(auto i: lst) s+= i;
    return s;
}


template<class T>
int sum_iL_tmpl(T s, std::initializer_list<int>lst){

    for(auto i: lst) s+= i;
    return s;
}


template<class... T>
auto sum_tmpl(const T&... args){
    return (args + ...);
}


int main(){

    std::string s1 = "aa ", s2 = "cc ", s3 = "bb ";

    std::cout << sum_c(4, 2, 3, 4, 5) << std::endl;
    std::cout << sum_iL({2, 3, 4, 5}) << std::endl;
    std::cout << sum_iL_tmpl(0, {2, 3, 4, 5}) << std::endl;
    std::cout << "------------------------------\n";
    std::cout << sum_tmpl(2, 3, 4, 5) << std::endl;
    std::cout << sum_tmpl(s1, s2, s3) << std::endl;

    return 0;
}
