#include <iostream>
#include <bitset>

void binToDec(int bin){
    int dec = 0;
    int base = 1;
    int temp = bin;

    while(temp){
        int lastDigit = temp % 10;
        temp = temp / 10;
        dec += lastDigit * base;
        base = base * 2;
    }
    std::cout << "Decimal form of " << bin << " is " << dec << "\n";
}

void decToBin(int n){
    int bin[100]; 
    int num = n;
    int i = 0;
    while (n > 0){
        bin[i] = n % 2;
        n = n / 2;
        ++i;
    }
        
    std::cout << "Binary form of " << num << " is ";
    for(int j = i -1; j >= 0; --j){
        std::cout << bin[j];
    }
    std::cout << "\n";
}

int main(){    
    int num;
    binToDec(10111011);
    decToBin(14);
    std::cout << "Input number: ";
    std::cin >> num;
    std::cout << "\n";
    std::cout << "Dec " << num << " in oct: " << std::oct << num << "\n";
    std::cout << "Dec " << num << " in dec: " << std::dec << num << "\n";
    std::cout << "Dec " << num << " in hex: " << std::hex << num << "\n";
    decToBin(num); 

    return 0;
}
