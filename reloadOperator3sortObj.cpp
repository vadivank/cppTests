#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <vector>

class Duration{
private:
    int hour;
    int min;
public:
    Duration(){}        // конструктор по-умолчанию (без него не работает)
    Duration(int hour_, int min_){
        int total = hour_ * 60 + min_;
        hour = total / 60 ;
        min = total % 60;
    }
    int Hour() const {
        return hour;
    }
    int Min() const {
        return min;
    }
};

bool operator<(const Duration& lhs, const Duration& rhs){
    if (lhs.Hour() == rhs.Hour()) {
        return lhs.Min() < rhs.Min();
    }
    return lhs.Hour() < rhs.Hour();
}

std::istream& operator>>(std::istream& in, Duration& duration){
    int hour;
    in >> hour;
    in.ignore(1);
    int min;
    in >> min;
    duration = Duration(hour, min);
    return in;    
}

std::ostream& operator<<(std::ostream& stream, const Duration& duration){
    stream << std::setfill('0');
    stream << std::setw(2) << duration.Hour() << ':'
            << std::setw(2) << duration.Min();
    return stream;
}

Duration operator+(const Duration& lhs, const Duration& rhs){
    return Duration{lhs.Hour() + rhs.Hour(), lhs.Min() + rhs.Min()};
}


int main(){
    std::vector<Duration> durations;
    durations.push_back(Duration{ 1, 50});
    durations.push_back(Duration{ 0, 50});
    durations.push_back(Duration{ 0,  1});
    for(const auto& dur : durations){
        std::cout << dur << "\t";
    }
    
    std::cout << std::endl;

    std::sort(durations.begin(), durations.end()); // не работает без перегрузки <

    for(const auto& dur : durations){
        std::cout << dur << "\t";
    }
    std::cout << std::endl;

    return 0;
}
