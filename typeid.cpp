#include <iostream>
#include <typeinfo>

auto GetA(int a) -> decltype(a + 3.0f){
    return a;
}

int main(){
    std::cout << GetA(2) << "\n";
    std::cout << sizeof(GetA(2)) << "\n";

    long long a = 1;
    std::cout << typeid(typeid(GetA(2)).name()).name() << "\n";

    return 0;
}
